//
//  AppManager.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 24/02/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

public typealias ManagerCompletionBlock = (_ result: Any, _ success: Bool, _ message: String) -> Void
public typealias ManagerAdditionBlock = (_ formData: MultipartFormData) -> Void

class AppManager: NSObject {
    // Temporary Hard coded, need to update based on login response.
    var currentCommunityID: Int = 0
    var objLoggedInUser:User? = nil;
    
    static let sharedInstance : AppManager = {
        let instance = AppManager()
        return instance
    }()
    
    override init()
    {
        super.init()
        self.initializeOnce()
    }
    
    func initializeOnce() {
        // Rechability
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        reachabilityManager?.listener = { status in
            
            switch status {
            case .notReachable:
                print("The network is not reachable")
                
            case .unknown:
                print("It is unknown whether the network is reachable")
                
            case .reachable(NetworkReachabilityManager.ConnectionType.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                
            case .reachable(NetworkReachabilityManager.ConnectionType.wwan):
                print("The network is reachable over the WWAN connection")
                
            }
        }
        // start listening
        reachabilityManager?.startListening()
//        if AppManager.getLoginArchivedObject(forKey: LOGGEDINUSER) != nil
//        {
//            let objUser: User? = (AppManager.getLoginArchivedObject(forKey: LOGGEDINUSER) as! User)
//            self.objLoggedInUser = objUser
//            var objUserCommunity: UserCommunityEntity? = objUser?.userCommunitys?.first
//            self.currentCommunityID = objUserCommunity?.community?.communityId
//        }
    }
    
    func callWsWithPOST(URL: String, completion: ManagerCompletionBlock) -> Void {
        
        //        completion(<#T##Any#>, <#T##Bool#>, <#T##String#>)
    }
    
    func callLoginWSWithPOST(parameters:Any ,completion: @escaping ManagerCompletionBlock) -> Void {
    }
    
    static func saveArchivedObject(_ object: Any, withKey strKey: String) {
        let ok = UnsafeMutablePointer<ObjCBool>.allocate(capacity: 1)
        ok[0] = true // or true
        let isDir = ok
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let dataPath = url.appendingPathComponent("\(AppManager.sharedInstance.currentCommunityID)")?.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: dataPath!, isDirectory: isDir) {
            print("Directory AVAILABLE")
        } else {
            print("Directory NOT AVAILABLE")
            do {
                try FileManager.default.createDirectory(atPath: dataPath!, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let appFile: String = URL(fileURLWithPath: dataPath!).appendingPathComponent(strKey).absoluteString
        
        if NSKeyedArchiver.archiveRootObject(object, toFile: appFile) {
            print("Successfully stored object at \(strKey)")
        }
        else {
            print("Failed to stored object at \(strKey)")
        }
    }
    
    static func getArchivedObject(forKey strKey: String) -> Any {
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = URL(fileURLWithPath: paths[0] as! String).appendingPathComponent("\(AppManager.sharedInstance.currentCommunityID)").absoluteString
        let appFile: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(strKey).absoluteString
        return NSKeyedUnarchiver.unarchiveObject(withFile: appFile)!
    }
    
    static func getLoginArchivedObject(forKey strKey: String) -> Any {
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = URL(fileURLWithPath: paths[0] as! String).absoluteString
        let appFile: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(strKey).absoluteString
        return NSKeyedUnarchiver.unarchiveObject(withFile: appFile)!
    }

    
    static func saveLoginArchivedObject(_ object: Any, withKey strKey: String) {
        let paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        let appFile = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(strKey)
        let data = NSKeyedArchiver.archivedData(withRootObject: object)

        do {
            try data.write(to: appFile)
        } catch {
            print("Couldn't write file")
        }

//        if NSKeyedArchiver.archiveRootObject(object, toFile: appFile) {
//            print("Successfully stored object at \(strKey)")
//        }
//        else {
//            print("Failed to stored object at \(strKey)")
//        }
    }
    
    static func navigationBarTheme(_ navigationController: UINavigationController) {
        navigationController.navigationBar.barTintColor = NAVIGATIONBAR_COLOR
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
        navigationController.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-500.0, 0), for: .default)
    }
}
