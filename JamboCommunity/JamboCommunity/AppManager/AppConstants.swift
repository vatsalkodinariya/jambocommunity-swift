//
//  AppConstants.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 24/02/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

let NAVIGATIONBAR_COLOR = UIColor(red: CGFloat(33.0 / 255.0), green: CGFloat(150.0 / 255.0), blue: CGFloat(243.0 / 255.0), alpha: CGFloat(1.0))
let LIGHT_GRAY_COLOR = UIColor(red: CGFloat(242.0 / 255.0), green: CGFloat(242.0 / 255.0), blue: CGFloat(242.0 / 255.0), alpha: CGFloat(1.0))
let LIGHT_RED_COLOR = UIColor(red: CGFloat(255.0 / 255.0), green: CGFloat(102.0 / 255.0), blue: CGFloat(102.0 / 255.0), alpha: CGFloat(1.0))
let GREEN_TRASPARENT_COLOR = UIColor(red: CGFloat(20.0 / 255.0), green: CGFloat(213.0 / 255.0), blue: CGFloat(80.0 / 255.0), alpha: CGFloat(0.8))
let LIGHT_GREENT_COLOR = UIColor(red: CGFloat(40.0 / 255.0), green: CGFloat(180.0 / 255.0), blue: CGFloat(80.0 / 255.0), alpha: CGFloat(1.0))
let LIGHT_BLUE_COLOR = UIColor(red: CGFloat(51.0 / 255.0), green: CGFloat(153.0 / 255.0), blue: CGFloat(200.0 / 255.0), alpha: CGFloat(1.0))
let WHITE_TRASPARENT_COLOR = UIColor(red: CGFloat(1.0), green: CGFloat(1.0), blue: CGFloat(1.0), alpha: CGFloat(0.4))

let APP_ALERT_TITLE = "Jambo"
let PAGESIZE = 14
let APPLICATIONURL = "https://itunes.apple.com/us/app/jambo-community/id1090395721?ls=1&mt=8"
let PARENT_SERVICE_URL = "http://35.154.100.203:8080/Jambo/" // TEST SERVER
//let PARENT_SERVICE_URL = "http://www.jambo.in/" // LIVE SERVER
//LIVE URL
//let PARENT_SERVICE_URL = "http://ec2-54-186-38-114.us-west-2.compute.amazonaws.com:8080/"

let DATABASENAME = "Jambo.sqlite"
let TABLECONVERSATION = "conversation"
let TABLECHATMESSAGE = "chatmessage"
let RECEIVED = "RECEIVED"
let SENT = "SENT"
let MESSAGERECEIVEDNOTIFICATION = "MessageReceivedNotification"
let MESSAGESENTNOTIFICATION = "MessageSentNotification"
let MESSAGEDELIVEREDNOTIFICATION = "MessageDeliveredNotification"
//#define PAGESIZE 15
let PAGECOUNTSDICT = "PageCountsDict"
let SUBSCRIBEDGROUPLIST = "SubscribedGroupList"
let ALLGROUPLIST = "AllGroupList"
let INVITEDGROUPLIST = "InvitedGroupList"
let REQUESTGROUPLIST = "RequestGroupList"
let ALLUSERSLIST = "AllUsersList"
let DISCUSSIONLIST = "DiscussionList"
let MYREFERREDUSERLIST = "MyReferredUserList"
let NEARBYUSERSLIST = "NearByUsersList"
let NOTIFICATIONLIST = "NotificationList"
let LOGGEDINUSER = "LoggedInUser"
let GPDCOURSELIST = "CourseList"
let GPDSKILLS = "Skills"
let GPDINDUSTRIES = "Industries"
let GPDTAGLIST = "TagList"
let GPDFUNCTIONSLIST = "FunctionsList"
let EVENTLIST = "EventList"
let MYEVENTLIST = "MyEventList"
let INVITEDEVENTLIST = "InvitedEventsList"
