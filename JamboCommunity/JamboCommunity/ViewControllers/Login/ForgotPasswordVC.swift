//
//  ForgotPasswordVC.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 01/03/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK: - IBActions
    @IBAction func forgotPassword(_ sender: Any) {
        
    }
    
    @IBAction func backTapHandler(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
