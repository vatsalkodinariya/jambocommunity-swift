//
//  LoginVC.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 24/02/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
import SWRevealViewController
import MessageUI
import MBProgressHUD
import Alamofire
import ObjectMapper
//import JSONModel

class LoginVC: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var loginView: UIView!
    var introScrollview: UIScrollView?
    @IBOutlet var btnCloseIntroduction: UIButton!
    @IBOutlet weak var btnTermsOfUse: UIButton!
    @IBOutlet weak var btnContactUs: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    weak var objProgressHud: MBProgressHUD!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
        txtEmail.text = "vikrant.zhala@bonoboz.in";
        txtPassword.text = "1142";
        
        AppDelegate.sharedInstance.navigationController = self.navigationController
        //        AppManager.sharedInstance.checkForForceUpdate()
        if UserDefaults.standard.bool(forKey: "IsIntroductionDone") {
            btnCloseIntroduction.isHidden = true
        }
        else {
            UserDefaults.standard.set(true, forKey: "IsIntroductionDone")
            UserDefaults.standard.synchronize()
            btnCloseIntroduction.isHidden = false
            self.displayIntroScreens()
        }
        btnContactUs.setTitle("Have issue in login?\n Contact us", for: .normal)
        btnContactUs.titleLabel?.numberOfLines = 2
        btnContactUs.titleLabel?.textAlignment = .center
        
        let attributedString = NSMutableAttributedString(string: "By Signing up, you agree to our \n Terms & Conditions")
        
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "Roboto-Light", size:14) as Any, range: NSRange(location: 0, length:"By Signing up, you agree to our \n Terms & Conditions".characters.count))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(white:0.9, alpha: 1.0), range: NSRange(location: 0, length:"By Signing up, you agree to our \n Terms & Conditions".characters.count))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "Roboto-Bold", size: 14) as Any, range: NSRange(location: "By Signing up, you agree to our \n ".characters.count, length: "Terms & Conditions".characters.count))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(white:0.9, alpha: 1.0), range: NSRange(location: "By Signing up, you agree to our \n ".characters.count, length: "Terms & Conditions".characters.count))
        //        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleThick, range: NSRange(location: "By Signing up, you agree to our \n ".characters.count, length: "Terms & Conditions".characters.count))
        //        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle, range: NSRange(location: 0, length: "Terms & Conditions".characters.count))
        
        //        let attributedString = NSMutableAttributedString(attributedString: attributedStringSignUp)
        //        attributedString .append(attributedStringNL)
        //        attributedString .append(attributedStringTNC)
        
        btnTermsOfUse.titleLabel?.numberOfLines = 0
        btnTermsOfUse.titleLabel?.textAlignment = .center
        btnTermsOfUse.setAttributedTitle(attributedString, for: .normal)
        if UserDefaults.standard.bool(forKey: "IsUserLogin") {
            //        [self performSegueWithIdentifier:@"RevealVCPush" sender:nil];
            let SWRevealVC: SWRevealViewController? = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController)
            SWRevealVC?.toggleAnimationType = SWRevealToggleAnimationType.easeOut
            SWRevealVC?.frontViewShadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(20.0))
            SWRevealVC?.springDampingRatio = 0.7
            SWRevealVC?.rearViewRevealOverdraw = 0
            self.navigationController?.pushViewController(SWRevealVC!, animated: false)
        }
    }
    
    func displayIntroScreens() {
        let height: CGFloat = self.view.frame.size.height
        let width: CGFloat = self.view.frame.size.width
        introScrollview = UIScrollView(frame: self.view.bounds)
        introScrollview?.contentSize = CGSize(width: CGFloat(width * 4), height: CGFloat(height))
        introScrollview?.isPagingEnabled = true
        introScrollview?.showsVerticalScrollIndicator = false
        introScrollview?.showsHorizontalScrollIndicator = false
        introScrollview?.backgroundColor = UIColor.white
        let img1 = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(width), height: CGFloat(height)))
        img1.image = UIImage(named: "1 discussions.png")
        introScrollview?.addSubview(img1)
        let img2 = UIImageView(frame: CGRect(x: CGFloat(width), y: CGFloat(0), width: CGFloat(width), height: CGFloat(height)))
        img2.image = UIImage(named: "2 interests.png")
        introScrollview?.addSubview(img2)
        let img3 = UIImageView(frame: CGRect(x: CGFloat(width * 2), y: CGFloat(0), width: CGFloat(width), height: CGFloat(height)))
        img3.image = UIImage(named: "3 nearby.png")
        introScrollview?.addSubview(img3)
        let img4 = UIImageView(frame: CGRect(x: CGFloat(width * 3), y: CGFloat(0), width: CGFloat(width), height: CGFloat(height)))
        img4.image = UIImage(named: "4 meetups.png")
        introScrollview?.addSubview(img4)
        self.view.addSubview(introScrollview!)
        btnCloseIntroduction.isHidden = false
        self.view.bringSubview(toFront: btnCloseIntroduction)
    }
    
    @IBAction func closeIntroduction(_ sender: Any) {
        introScrollview?.removeFromSuperview()
        btnCloseIntroduction.isHidden = true
        //    [self checkAuthenticationStatus];
    }
    
    @IBAction func loginHandler(_ sender: UIButton) {
        if txtEmail.text?.characters.count == 0 {
            let alert = UIAlertController(title: APP_ALERT_TITLE, message: "Email ID should not be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else if txtPassword.text?.characters.count == 0 {
            let alert = UIAlertController(title: APP_ALERT_TITLE, message: "Password should not be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else {
            txtEmail.resignFirstResponder()
            txtPassword.resignFirstResponder()
            // Save login user email in userdefaults
            UserDefaults.standard.set(txtEmail.text, forKey: "LoginUserEmail")
            objProgressHud = MBProgressHUD.showAdded(to: self.view, animated: true)
            objProgressHud.mode = MBProgressHUDMode.indeterminate
            objProgressHud.animationType = MBProgressHUDAnimation.fade
            objProgressHud.dimBackground = true
            objProgressHud.labelText = "Signing in..."
            
            
            
            //    ObjProgressHud.backgroundView.backgroundColor = [UIColor colorWithWhite:0.f alpha:.3f];
            //    ObjProgressHud.label.text = @"Signing in...";
            
            
            let jsonObject: [String: Any] = [
                "userName" : txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                "password" : txtPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let headers: HTTPHeaders = [
                "OS": "IOS",
                "Accept": "application/json",
                "Content-Type": "application/json",
                "deviceID": (UIDevice.current.identifierForVendor?.uuidString)!,
                "gcmID": AppDelegate.sharedInstance.strDeviceToken!
            ]
            
            
            Alamofire.request(PARENT_SERVICE_URL + "user/login", method: .post, parameters: jsonObject, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                self.objProgressHud.hide(true)
                if (response.value as! NSDictionary).object(forKey: "responseCode") as! Int == 1000
                {
                    let value = (response.value as! NSDictionary).object(forKey: "view")
                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: value as Any,
                        options: []) {
                        let theJSONText = String(data: theJSONData,
                                                 encoding: .ascii)
                        print("JSON string = \(theJSONText!)")
                        switch response.result {
                        case .success:
                            let user = Mapper<User>().map(JSONString: theJSONText!)
//                            let user = try? User(string: theJSONText!,error: nil)
//                            let user = try? User(dictionary: value as? [AnyHashable:Any])
                            user?.password = self.txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                            let Headers : [AnyHashable:Any] = (response.response?.allHeaderFields)!
                            user?.sessionid = Headers[AnyHashable("sessionid")] as? String
                            AppManager.sharedInstance.objLoggedInUser = user
                            AppManager.saveLoginArchivedObject(user as Any, withKey: LOGGEDINUSER)
                            UserDefaults.standard.set(true, forKey: "IsUserLogin")
                            UserDefaults.standard.synchronize()
                            break
                        case .failure(let error):
                            print("Fetch failed :( error: \(error.localizedDescription)")
                            switch(error._code) {
                            case NSURLErrorTimedOut:
                                print("Timed Out")
                            default:
                                print(error._code)
                            }
                        }

                    }
                }
                else
                {
                    UIAlertView(title: APP_ALERT_TITLE, message: (response.value as! NSDictionary).object(forKey: "message") as! String, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "Ok").show()
                }
            })
            
            
            //            AppManager.sharedInstance.callWSToLogin(withPOSTUrl: "\(PARENT_SERVICE_URL)\("user/login")", withParams: dictTemp, withCompletion: {(_ result: Any, _ success: Bool, _ message: String) -> Void in
            //                ObjProgressHud.hide(true)
            //                if success {
            //                    var error: Error? = nil
            //                    var objUser = (try? UserEntity(dictionary: result))
            //                    objUser?.password = txtPassword.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            //                    AppManager.saveLoginArchivedObject(objUser, withKey: LOGGEDINUSER)
            //                    AppManager.sharedInstance().objLoggedInUser = objUser
            //                    var objUserCommunity: UserCommunityEntity? = objUser?.userCommunitys?.first
            //                    AppManager.sharedInstance().currentCommunityID = objUserCommunity?.community?.communityId
            //                    UserDefaults.standard.set(true, forKey: "IsUserLogin")
            //                    UserDefaults.standard.synchronize()
            //                    var SWRevealVC: SWRevealViewController? = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController)
            //                    SWRevealVC?.toggleAnimationType = SWRevealToggleAnimationTypeEaseOut
            //                    SWRevealVC?.frontViewShadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(20.0))
            //                    SWRevealVC?.springDampingRatio = 0.7
            //                    SWRevealVC?.rearViewRevealOverdraw = 0
            //                    self.navigationController?.pushViewController(SWRevealVC, animated: true)
            //                }
            //                else {
            //                    Alerts.showAlert(with: APP_ALERT_TITLE, andMessage: message, withBlock: nil, andButtons: "Ok", nil)
            //                }
            //            })
        }
        
    }
    
    @IBAction func actionBtnTermsOfUse(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "http://www.jambo.in/terms-of-use")!) {
            UIApplication.shared.openURL(URL(string: "http://www.jambo.in/terms-of-use")!)
        }
    }
    // MARK: - TextField Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
            return false
        }
        if (txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count)! > 0 && (txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count)! > 0 {
            self.loginHandler(btnLogin)
        }
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func actionBtnForgotPassword(_ sender: UIButton) {
        self.performSegue(withIdentifier: "forgotPasswordVCPush", sender: nil)
    }
    
    @IBAction func actionBtnContactUs(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            // opem email sheet
            let controller = MFMailComposeViewController()
            controller.mailComposeDelegate = self
            controller.setSubject("Problem with signing in application.")
            controller.setMessageBody("", isHTML: false)
            controller.setToRecipients(["info@jambo.in"])
//            if controller {
                self.navigationController?.present(controller, animated: true, completion: { _ in })
//            }
        }
        else {
            UIAlertView(title: APP_ALERT_TITLE, message: "Can not send email. Kindly configure email account", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "").show()
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if result == .sent {
            print("It's away!")
        }
        self.navigationController?.dismiss(animated: true, completion: { _ in })
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
}
