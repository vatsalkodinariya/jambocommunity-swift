
#import "Alerts.h"
@implementation Alerts

+(Alerts*)sharedInstance
{
    static Alerts *objAlert = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objAlert = [[self alloc] init];
    });
    return objAlert;
}

+(void)showAlertWithTitle:(NSString*)pstrTitle andMessage:(NSString *)pstrMessage withBlock:(actionBlock)pobjBlock andButtons:(NSString*)pstrButton, ...NS_REQUIRES_NIL_TERMINATION
{
    Alerts *objAlerts = [Alerts sharedInstance];
    objAlerts.pBlock = pobjBlock;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:pstrTitle message:pstrMessage delegate:objAlerts cancelButtonTitle:nil otherButtonTitles:nil];
    
    va_list strButtonNameList;
    va_start(strButtonNameList, pstrButton);
    int index=0, pos = 0;
    for (NSString *strBtnTitle = pstrButton; strBtnTitle != nil; strBtnTitle = va_arg(strButtonNameList, NSString*))
    {
        [alert addButtonWithTitle:strBtnTitle];
        if([strBtnTitle.lowercaseString isEqualToString:@"cancel"])
            pos = index;
        index++;
    }
    va_end(strButtonNameList);
    [alert setCancelButtonIndex:pos];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
        [alert show];
}


+(UIActionSheet *)actionSheetWithBlock:(actionBlock)pobjBlock withTitle:(NSString *)strTitle andButtons:(NSString*)pstrButton, ...NS_REQUIRES_NIL_TERMINATION
{
    Alerts *objAlerts = [Alerts sharedInstance];
    objAlerts.pBlock = pobjBlock;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:strTitle delegate:objAlerts cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    va_list strButtonNameList;
    va_start(strButtonNameList, pstrButton);
    for (NSString *strBtnTitle = pstrButton; strBtnTitle != nil; strBtnTitle = va_arg(strButtonNameList, NSString*))
    {
        if(![[strBtnTitle lowercaseString] isEqualToString:@"cancel"])
            [actionSheet addButtonWithTitle:strBtnTitle];
    }
    va_end(strButtonNameList);
    
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:actionSheet.numberOfButtons-1];
    
    return actionSheet;
}

#pragma mark - Delegates

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.pBlock)
        self.pBlock(buttonIndex);
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.pBlock)
        self.pBlock(buttonIndex);
}

@end
