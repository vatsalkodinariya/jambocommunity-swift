
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^actionBlock)(NSInteger buttonIndex);

@interface Alerts : NSObject <UIAlertViewDelegate,UIActionSheetDelegate>

@property (nonatomic,copy) actionBlock pBlock;

//+(void)showAlertWithMessage:(NSString*)pstrMessage withBlock:(actionBlock)pobjBlock andButtons:(NSString*)pstrButton, ...NS_REQUIRES_NIL_TERMINATION;
+(UIActionSheet *)actionSheetWithBlock:(actionBlock)pobjBlock withTitle:(NSString *)strTitle andButtons:(NSString*)pstrButton, ...NS_REQUIRES_NIL_TERMINATION;
+(void)showAlertWithTitle:(NSString*)pstrTitle andMessage:(NSString *)pstrMessage withBlock:(actionBlock)pobjBlock andButtons:(NSString*)pstrButton, ...NS_REQUIRES_NIL_TERMINATION;

@end
