//
//  User.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 01/03/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
/*
import JSONModel
class User: JSONModel {
    var userId: NSNumber!
    var userCommunitys: [UserCommunity]?
    var lastName: String?
    var firstName: String?
    var createDate: String?
    var thumbNailProfilePic: String?
    var longitude: String?
    var latitude: String?
    var communityId: String?
    var userName : String?
    var password: String?
    var createdBy: String?
    var updateDate: String?
    var fullName: String?
    var userDetail: String?
    var updatedBy: String?
    var profilePic: String?
    var sessionid: String?
    var activationStatus: NameID?

    override class func keyMapper() -> JSONKeyMapper! {
        //code here
        return JSONKeyMapper(modelToJSONDictionary: ["userId": "id"])
    }
    
    override class func propertyIsOptional(_ propertyName: String!) -> Bool {
        if propertyName == "sessionid"
        {
            return true
        }
        return false
    }}
*/
import ObjectMapper
class User: NSObject, NSCoding, Mappable {
    
    var userId: Int? = 0
    var userCommunitys: [UserCommunity]?
    var lastName: String? = ""
    var firstName: String? = ""
    var createDate: String? = ""
    var thumbNailProfilePic: String? = ""
    var longitude: String? = ""
    var latitude: String? = ""
    var communityId: String? = ""
    var userName : String? = ""
    var password: String? = ""
    var createdBy: String? = ""
    var updateDate: String? = ""
    var fullName: String? = ""
    var userDetail: String? = ""
    var updatedBy: String? = ""
    var profilePic: String? = ""
    var sessionid: String? = ""
    var activationStatus: NameID?

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        userId <- map["id"]
        userCommunitys <- map["userCommunitys"]
        lastName <- map["lastName"]
        firstName <- map["firstName"]
        createDate <- map["createDate"]
        thumbNailProfilePic <- map["thumbNailProfilePic"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        communityId <- map["communityId"]
        userName  <- map["userName"]
        password <- map["password"]
        createdBy <- map["createdBy"]
        updateDate <- map["updateDate"]
        fullName <- map["fullName"]
        userDetail <- map["userDetail"]
        updatedBy <- map["updatedBy"]
        profilePic <- map["profilePic"]
        sessionid <- map["sessionid"]
        activationStatus <- map["activationStatus"]
    }
    
    // MARK: NSCoding
    public required init?(coder aDecoder: NSCoder) {
        userId = aDecoder.decodeInteger(forKey: "userId")
        userCommunitys = aDecoder.decodeObject(forKey: "userCommunitys") as! [UserCommunity]?
        lastName = aDecoder.decodeObject(forKey: "lastName") as! String?
        firstName = aDecoder.decodeObject(forKey: "firstName") as! String?
        createDate = aDecoder.decodeObject(forKey: "createDate") as! String?
        thumbNailProfilePic = aDecoder.decodeObject(forKey: "thumbNailProfilePic") as! String?
        longitude = aDecoder.decodeObject(forKey: "longitude") as! String?
        latitude = aDecoder.decodeObject(forKey: "latitude") as! String?
        communityId = aDecoder.decodeObject(forKey: "communityId") as! String?
        userName = aDecoder.decodeObject(forKey: "userName") as! String?
        password = aDecoder.decodeObject(forKey: "password") as! String?
        createdBy = aDecoder.decodeObject(forKey: "createdBy") as! String?
        updateDate = aDecoder.decodeObject(forKey: "updateDate") as! String?
        fullName = aDecoder.decodeObject(forKey: "fullName") as! String?
        userDetail = aDecoder.decodeObject(forKey: "userDetail") as! String?
        updatedBy = aDecoder.decodeObject(forKey: "updatedBy") as! String?
        profilePic = aDecoder.decodeObject(forKey: "profilePic") as! String?
        sessionid = aDecoder.decodeObject(forKey: "sessionid") as! String?
        activationStatus = aDecoder.decodeObject(forKey: "activationStatus") as! NameID?
    }

    
    public func encode(with aCoder: NSCoder) {
        aCoder.encodeCInt(Int32(userId!), forKey: "userId")
        aCoder.encode(userCommunitys, forKey: "userCommunitys")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(createDate, forKey: "createDate")
        aCoder.encode(thumbNailProfilePic, forKey: "thumbNailProfilePic")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(communityId, forKey: "communityId")
        aCoder.encode(userName, forKey: "userName")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(createdBy, forKey: "createdBy")
        aCoder.encode(updateDate, forKey: "updateDate")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(userDetail, forKey: "userDetail")
        aCoder.encode(updatedBy, forKey: "updatedBy")
        aCoder.encode(profilePic, forKey: "profilePic")
        aCoder.encode(sessionid, forKey: "sessionid")
        aCoder.encode(activationStatus, forKey: "activationStatus")

    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeCInt(Int32(userId!), forKey: "userId")
        coder.encode(userCommunitys, forKey: "userCommunitys")
        coder.encode(lastName, forKey: "lastName")
        coder.encode(firstName, forKey: "firstName")
        coder.encode(createDate, forKey: "createDate")
        coder.encode(thumbNailProfilePic, forKey: "thumbNailProfilePic")
        coder.encode(longitude, forKey: "longitude")
        coder.encode(latitude, forKey: "latitude")
        coder.encode(communityId, forKey: "communityId")
        coder.encode(userName, forKey: "userName")
        coder.encode(password, forKey: "password")
        coder.encode(createdBy, forKey: "createdBy")
        coder.encode(updateDate, forKey: "updateDate")
        coder.encode(fullName, forKey: "fullName")
        coder.encode(userDetail, forKey: "userDetail")
        coder.encode(updatedBy, forKey: "updatedBy")
        coder.encode(profilePic, forKey: "profilePic")
        coder.encode(sessionid, forKey: "sessionid")
        coder.encode(activationStatus, forKey: "activationStatus")
    }

}
