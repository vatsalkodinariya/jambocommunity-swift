//
//  UserCommunity.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 04/03/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
/*
import JSONModel
class UserCommunity: JSONModel {
    var community: Community?
    var user : String?
    var role : NameID?
    var status: NameID?
    var userId: String?
    var communityId: String?
//    override class func keyMapper() -> JSONKeyMapper! {
//        //code here
//        return JSONKeyMapper(modelToJSONDictionary: ["IDNameEntityid": "id"])
//    }

}
*/
import ObjectMapper
class UserCommunity: NSObject, NSCoding, Mappable {

    
    var community: Community?
    var user : String? = ""
    var role : NameID?
    var status: NameID?
    var userId: String? = ""
//    var communityId: String? = ""

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        community <- map["community"]
        user <- map["user"]
        role <- map["role"]
        status <- map["status"]
        userId <- map["userId"]
//        communityId <- map["communityId"]
    }
    
    // MARK: NSCoding
    public required init?(coder aDecoder: NSCoder) {
        community = aDecoder.decodeObject(forKey: "community") as! Community?
        user = aDecoder.decodeObject(forKey: "user") as! String?
        role = aDecoder.decodeObject(forKey: "role") as! NameID?
        status = aDecoder.decodeObject(forKey: "status") as! NameID?
        userId = aDecoder.decodeObject(forKey: "name") as! String?
//        communityId = aDecoder.decodeObject(forKey: "communityId") as! String?
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(community, forKey: "community")
        aCoder.encode(user, forKey: "user")
        aCoder.encode(role, forKey: "role")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(userId, forKey: "userId")
//        aCoder.encode(communityId!, forKey: "communityId")

    }

    
    func encodeWithCoder(coder: NSCoder) {
        coder.encode(community, forKey: "community")
        coder.encode(user, forKey: "user")
        coder.encode(role, forKey: "role")
        coder.encode(status, forKey: "status")
        coder.encode(userId, forKey: "userId")
//        coder.encode(communityId!, forKey: "communityId")
    }

}
