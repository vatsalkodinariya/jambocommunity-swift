//
//  Community.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 04/03/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
/*
import JSONModel
class Community: JSONModel {
    var communityId: Int?
    var createDate : String?
    var createdBy : String?
    var updateDate : String?
    var updatedBy : String?
    var activationStatus : String?
    var name : String?
    var communityType: NameID?
    var groupType: NameID?

    override class func keyMapper() -> JSONKeyMapper! {
        //code here
        return JSONKeyMapper(modelToJSONDictionary: ["communityId": "id"])
    }

}
*/
import ObjectMapper
class Community: NSObject, NSCoding, Mappable {
    
    var communityId: Int? = 0
    var createDate : String? = ""
    var createdBy : String? = ""
    var updateDate : String? = ""
    var updatedBy : String? = ""
    var activationStatus : String? = ""
    var name : String? = ""
    var communityType: NameID?
    var groupType: NameID?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        communityId <- map["id"]
        createDate <- map["createDate"]
        createdBy <- map["createdBy"]
        updateDate <- map["updateDate"]
        activationStatus <- map["activationStatus"]
        name <- map["name"]
        communityType <- map["communityType"]
        groupType <- map["groupType"]
    }

    // MARK: NSCoding
//    required convenience init(coder decoder: NSCoder) {
//        
//        communityId = decoder.decodeInteger(forKey: "communityId")
//        createDate = decoder.decodeObject(forKey: "createDate") as! String?
//        createdBy = decoder.decodeObject(forKey: "createdBy") as! String?
//        updateDate = decoder.decodeObject(forKey: "updateDate") as! String?
//        activationStatus = decoder.decodeObject(forKey: "activationStatus") as! String?
//        name = decoder.decodeObject(forKey: "name") as! String?
//        communityType = decoder.decodeObject(forKey: "communityType") as! NameID?
//        groupType  = decoder.decodeObject(forKey: "communityType") as! NameID?
//
//    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encodeCInt(Int32(self.communityId!), forKey: "communityId")
        aCoder.encode(createDate, forKey: "createDate")
        aCoder.encode(createdBy, forKey: "createdBy")
        aCoder.encode(updateDate, forKey: "updateDate")
        aCoder.encode(activationStatus, forKey: "activationStatus")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(communityType!, forKey: "communityType")
        aCoder.encode(groupType!, forKey: "groupType")
        
    }

    public required init?(coder aDecoder: NSCoder) {
        communityId = aDecoder.decodeInteger(forKey: "communityId")
        createDate = aDecoder.decodeObject(forKey: "createDate") as! String?
        createdBy = aDecoder.decodeObject(forKey: "createdBy") as! String?
        updateDate = aDecoder.decodeObject(forKey: "updateDate") as! String?
        activationStatus = aDecoder.decodeObject(forKey: "activationStatus") as! String?
        name = aDecoder.decodeObject(forKey: "name") as! String?
        communityType = aDecoder.decodeObject(forKey: "communityType") as! NameID?
        groupType  = aDecoder.decodeObject(forKey: "communityType") as! NameID?
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeCInt(Int32(self.communityId!), forKey: "communityId")
        coder.encode(createDate, forKey: "createDate")
        coder.encode(createdBy, forKey: "createdBy")
        coder.encode(updateDate, forKey: "updateDate")
        coder.encode(activationStatus, forKey: "activationStatus")
        coder.encode(name, forKey: "name")
        coder.encode(communityType!, forKey: "communityType")
        coder.encode(groupType!, forKey: "groupType")
    }
}
