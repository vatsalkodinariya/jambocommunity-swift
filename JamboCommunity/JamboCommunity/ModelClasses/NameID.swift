//
//  NameID.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 04/03/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
/*
import JSONModel
class NameID: JSONModel {
    var IDNameEntityid: Int?
    var name : String?
    
    override class func keyMapper() -> JSONKeyMapper! {
        //code here
        return JSONKeyMapper(modelToJSONDictionary: ["IDNameEntityid": "id"])
    }

}
*/
import ObjectMapper
class NameID: NSObject, NSCoding, Mappable {

    
    var IDNameEntityid: Int? = 0
    var name : String? = ""
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        IDNameEntityid <- map["id"]
        name <- map["name"]
    }
    
    // MARK: NSCoding
    public required init?(coder aDecoder: NSCoder) {
        IDNameEntityid = aDecoder.decodeInteger(forKey: "IDNameEntityid")
        name = aDecoder.decodeObject(forKey: "name") as! String?
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encodeCInt(Int32(IDNameEntityid!), forKey: "IDNameEntityid")
        aCoder.encode(name, forKey: "name")
    }

    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeCInt(Int32(IDNameEntityid!), forKey: "IDNameEntityid")
        coder.encode(name, forKey: "name")
    }
}
