//
//  AppDelegate.swift
//  JamboCommunity
//
//  Created by Vatsal Kodinariya on 23/02/17.
//  Copyright © 2017 Bonoboz Marketing Services Private Limited. All rights reserved.
//

import UIKit
import CoreLocation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController?
    var locationManager: CLLocationManager?
    var strDeviceToken: String?
    var currentLocation: CLLocation?
    var isLocationDenied: Bool?
    var currentCity: String?
    var currentCountry: String?
    
    static let sharedInstance : AppDelegate = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate
        
    }()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        callImportantMethods()
//        AppManager.sharedInstance.initializeOnce();
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
// MARK: - Important Methods
    func callImportantMethods() {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
        
    }

    // MARK: - Remote Notification Methods
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = "\(deviceToken)"
        token = token.replacingOccurrences(of: "<", with: "")
        token = token.replacingOccurrences(of: ">", with: "")
        token = token.replacingOccurrences(of: " ", with: "")
        self.strDeviceToken = token
        print("My token is: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to get token, error: \(error)")
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            self.strDeviceToken = "XXXXX000000"
        #endif
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("Remote Notification \(userInfo.description)")
        //    [Alerts showAlertWithTitle:APP_ALERT_TITLE andMessage:[userInfo description] withBlock:nil andButtons:@"Ok", nil];
        if application.applicationState == .active {
            //        [self checkForBackGroundViewUpdate:userInfo];
//            Alerts.showAlert(with: APP_ALERT_TITLE, andMessage: ((userInfo.value(forKey: "aps") as? String)?.value(forKey: "alert") as? String), withBlock: {(_ buttonIndex: Int) -> Void in
//                if buttonIndex == 0 {
                    //                [self redirectToScreen:userInfo];
//                }
//            }, andButtons: "Ok", "Cancel", nil)
        }
        else {
            //        [self redirectToScreen:userInfo];
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    
    func copyDataBaseFile() {
        var success: Bool
        let fileManager = FileManager.default
        let error: Error?
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = paths[0] as! String
        let writableDBPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(DATABASENAME).absoluteString
        success = fileManager.fileExists(atPath: writableDBPath)
        if success {
            return
        }
        // The writable database does not exist, so copy the default to the appropriate location.
        let defaultDBPath: String = URL(fileURLWithPath: (Bundle.main.resourcePath)!).appendingPathComponent(DATABASENAME).absoluteString
        success = ((try? fileManager.copyItem(atPath: defaultDBPath, toPath: writableDBPath)) != nil)
        if !success {
//            assert(false, "Failed to create writable database file with message '\(error?.localizedDescription)'.")
        }
    }
    
    class func databasePath() -> String {
        let pathForDB: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = pathForDB[0] as! String
        let finalPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(DATABASENAME).absoluteString
        return finalPath
    }
    
    func application(_ application: UIApplication, didChangeStatusBarFrame oldStatusBarFrame: CGRect) {
        self.window?.layoutIfNeeded()
        self.window?.updateConstraintsIfNeeded()
    }
    
    func application(_ application: UIApplication, willChangeStatusBarFrame newStatusBarFrame: CGRect) {
        self.window?.layoutIfNeeded()
        self.window?.updateConstraintsIfNeeded()
    }
}

